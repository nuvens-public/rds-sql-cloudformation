# RDS-SQL CloudFormation

Deploy a MS-SQL database on RDS for WSM. There are some variables that need to be modified:
* AllocatedStorage: initial size set to 20, it can be used as is for Dev and Prod
* DBInstanceClass: recommended an 8GB instance, but also 4 GB can be used for testing
* Engine: SQL version to use (Enterprise, Standard, Web or Express)
* KmsKeyId: requires the ARN value of existing encryption keys in KMS
* MasterUsername: defaults to `dbadmin`, but it is configurable
* SubnetA: existing Subnet in one of the availability zone
* SubnetB: existing Subnet a second availability zone
* VPC: existing VPC in which the Database will be deployed

The password will be created randomly by the deployment and saved in AWS Secrets Manager.